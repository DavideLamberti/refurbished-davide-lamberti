// chiamata asincrona per il file annunci.json, viene sempre letto per ultima
// istruzione rispetto alle altre

fetch('./annunci.json')
    .then(response => response.json())
    .then(data => {


        // funzione per aggiungere card annunci
        function populateAds(ads) {
            const announcementWrapper = document.querySelector('#announcementWrapper')

            announcementWrapper.innerHTML = '';

            ads.forEach(announcement => {
                let card = document.createElement('div')

                card.classList.add('col-12', 'col-md-6')

                card.innerHTML =
                    `
                    <div class="card-announcement-list shadow">
                        <div class="row">
                            <div class="col-12 col-xl-6 img-box">
                                <img class="img-fluid" src="./img/rogphone.png" alt="">
                            </div>
                            <div class="col-12 col-xl-6 details-box">
                                <h3 class="mt-3 fs-1">${announcement.name}</h3>
                                <h4 class="fs-5">${announcement.category}</h4>
                                <p>Descrivi il tuo prodotto</p>
                                <div class="mb-2">
                                    <div>
                                        <p class="mb-0 fs-4 fw-bold">${announcement.price}€</p>
                                    </div>
                                    <div class="mt-0">
                                        <button class="btn-danger rounded-pill">Dettagli</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>               
                `
                announcementWrapper.appendChild(card)
            });
        }

        // funzione per aggiungere checkbox categorie
        function populateCategoriesFilter() {

            let categories = new Set(data.map(ad => ad.category))
            const categoriesFilterWrapper = document.querySelector('#categoriesFilterWrapper')

            categories.forEach(category => {
                let check = document.createElement('div');

                check.classList.add('form-check')

                check.innerHTML =
                    `
                <input class="form-check-input filterCategory" type="checkbox" value="${category}" id="checkbox${category}">
                <label class="form-check-label" for="checkbox${category}">
                ${category}
                </label>
                `
                categoriesFilterWrapper.appendChild(check);
            })
        }
        // filtro per categorie
        function attachFilterCategoryEvent() {
            let checks = document.querySelectorAll('.filterCategory')

            checks.forEach(check => {
                check.addEventListener('input', () => {

                    let checkedCategories = Array.from(checks)
                        .filter(check => check.checked)
                        .map(check => check.value)

                    let filteredAds = data.filter(ad => checkedCategories.includes(ad.category))

                    if (filteredAds.length == 0) {
                        populateAds(data)
                    } else {
                        populateAds(filteredAds);
                    }
                })
            })
        }

        // filtro per ricerca
        function filterBySearch(search) {
            let filtered = data.filter(ad => {
                return ad.name.toLowerCase().includes(search.toLowerCase())
            })
            populateAds(filtered)
        }
        // filtro con range di prezzo
        function populateFilterByPrice() {
            let prices = data.map(ad => Number(ad.price));
            let sorted = prices.sort((a, b) => a - b)
            let min = Math.floor(sorted[0])
            let max = Math.ceil(sorted[sorted.length - 1])

            priceInputs.forEach(input => {
                input.max = max
                input.min = min
            })
            priceInputs[0].value = min
            priceInputs[1].value = max

            rangeValues.innerHTML = `${min[0]}€ - ${max[1]}€`
        }
        // filtrare effettivamente per prezzo
        function filterByPrice(min, max) {
            let filtered = data.filter(ad => {
                if (Number(ad.price) <= max && Number(ad.price) >= min) {
                    return true
                } else {
                    return false
                }
            })
            console.log(filtered)
        }


        const searchInput = document.querySelector('#searchInput')
        const priceInputs = document.querySelectorAll('.priceInput')
        const rangeValues = document.querySelectorAll('.rangeValues')

        searchInput.addEventListener('input', () => {
            filterBySearch(searchInput.value)
        })

        priceInputs.forEach(priceInput => {
            priceInput.addEventListener('input', () => {

                let values = Array.from(priceInputs).map(priceInput => priceInput.value).sort((a, b) => a - b)

                rangeValues.innerHTML = `${values[0]}€ - ${values[1]}€`
                filterByPrice(values[0], values[1])
            })
        })








        populateFilterByPrice()
        populateCategoriesFilter()
        attachFilterCategoryEvent()
        populateAds(data)
    })